# User documentation


## Work published

* On Robot Compliance: A Cerebellar Control Approach: https://ieeexplore-ieee-org.focus.lib.kth.se/abstract/document/8880621
* Scalability in Neural Control of Musculoskeletal Robots: https://arxiv.org/abs/1601.04862

 
## Other references:

* An adaptive filter model of cerebellar zone C3 as a basis for safe limb control? https://physoc.onlinelibrary.wiley.com/doi/full/10.1113/jphysiol.2013.261545
* Adaptive filters and internal models: Multilevel description of cerebellar function: https://www.sciencedirect.com/science/article/pii/S0893608012003206?via%3Dihub
* Cerebellar-inspired algorithm for adaptive control of nonlinear dielectric elastomer-based artificial muscle: https://royalsocietypublishing.org/doi/10.1098/rsif.2016.0547
* Biohybrid control of general linear systems using the adaptive filter model of cerebellum: https://www.frontiersin.org/articles/10.3389/fnbot.2015.00005/full

